document.getElementById("btn-nguyen-duong").addEventListener("click", function() {
    var n = 0;
    var S = 0;
    while (S < 10000) {
        n++;
        S = S + n;
    }
    document.getElementById("kq-nguyen-duong").value = "Số nguyên dương nhỏ nhất là: " + n;
});
document.getElementById("btn-tinh-tong").addEventListener("click", function() {
    var x = document.getElementById("so-x").value * 1;
    var n = document.getElementById("so-n").value * 1;
    var i = 1;
    var T = 1;
    var S = 0;
    while (i <= n) {
        T = T * x;
        S = S + T;
        i++;

    }
    document.getElementById("kq-tinh-tong").value = S;

});
document.getElementById("btn-giai-thua").addEventListener("click", function() {
    var n = document.getElementById("n").value * 1;
    var i = 1;
    var S = 1;
    for (i; i <= n; i++) {
        S = i * S;
    }
    document.getElementById("kq-giai-thua").value = S;
});



function redDiv(index) {
    return `<div class = "red-div">${index} Div Chẵn</div>`;
}

function blueDiv(index) {
    return `<div class = "blue-div">${index} Div Lẻ</div>`;
}
document.getElementById("btn-in-div").addEventListener("click", function() {
    var i = 1;
    var content = "";
    for (i = 1; i <= 10; i++) {
        if (i % 2 == 0) {
            content += redDiv(i);
        } else {
            content += blueDiv(i);
        }
        document.getElementById("kq-in-div").innerHTML = content;
    }
});